-- enable spell checking
-- these enable spell checking globally, I can't find a way to enable it locally
-- vim.opt.spell = true
-- vim.wo.spell = true
-- vim.bo.spelllang = "en_us"
-- TODO: refer to typst style/browser for width
-- vim.bo.textwidth = 80
-- vim.wo.colorcolumn = "80"
-- Use treesitter for folding
vim.wo.foldmethod = "expr"
-- Set tabs to soft 2-space
vim.bo.tabstop = 2
vim.bo.softtabstop = 2
vim.bo.shiftwidth = 2
vim.bo.expandtab = true
