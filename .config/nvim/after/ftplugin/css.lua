-- Use treesitter for folding
vim.wo.foldmethod = "indent"
-- Set tabs to soft 2-space
vim.bo.tabstop = 2
vim.bo.softtabstop = 2
vim.bo.shiftwidth = 2
vim.bo.expandtab = true
