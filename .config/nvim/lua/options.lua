-- @file $XDG_CONFIG_HOME/lua/custom/init.lua
--
-- Neovim options to be loaded by NvChad.
--
-- @author J. Nolan Faught <nagefire.dev@gmail.com>
-- @date 4 June 2024
require "nvchad.options"

-- Assume that .h files are C headers
vim.g.c_syntax_for_h = 1

-- Basic text and editor display settings
vim.opt.encoding = "UTF-8"
vim.opt.wrap = false
vim.opt.termguicolors = true

-- Allow for buffers to stay open when not in a window
vim.opt.hidden = true

-- Enable incremental search
vim.opt.incsearch = true

-- Prompt to reload the file if external changes are detected
vim.opt.autoread = true

-- Set the default shell
vim.opt.shell = "/bin/bash"

-- Default indentation, uses 8-space hard tabs
vim.opt.tabstop = 8
vim.opt.softtabstop = 8
vim.opt.shiftwidth = 8
vim.opt.expandtab = false
vim.opt.smarttab = true
vim.opt.autoindent = true

-- Set backspace behavior to update the buffer
vim.opt.backspace = { "indent", "eol", "start" }

-- Display whitespace
vim.opt.listchars = "tab:  ,trail:$"
vim.opt.list = true
-- Enable relative line numbers and display end col
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.colorcolumn = "80"
vim.opt.signcolumn = "yes"
vim.opt.textwidth = 80
-- Fold settings
vim.wo.foldmethod = "indent"
vim.wo.foldexpr = "nvim_treesitter#foldexpr()"
vim.wo.foldnestmax = 5

-- Filetype detection for hyprland
vim.filetype.add({
	pattern = { [".*/hypr/.*%.conf"] = "hyprlang" },
})

-- Cscope and ctags integration
-- vim.opt.cscopetag = true

-- Don't pass messages to `ins-completion-menu`
-- vim.opt.shortmess = vim.opt.shortmess .. 'c'

-- Language providers
vim.g.python3_host_prog = "/usr/bin/python3"
vim.g.loaded_python_provider = false
vim.g.loaded_ruby_provider = false
vim.g.loaded_perl_provider = false
-- Remove command line
-- vim.opt.cmdheight = 0
