-- @file $XDG_CONFIG_HOME/nvim/lua/plugins/init.lua
--
-- Custom plugin configuration for NvChad.
--
-- @author J. Nolan Faught <nagefire.dev@gmail.com>
-- @date 24 July 2024
overrides = require "configs.overrides"

local plugins = {
  -- disable mason
  { "williamboman/mason.nvim", enabled = false },
  -- Language server integration
  {
    "neovim/nvim-lspconfig",
    config = function()
      require("nvchad.configs.lspconfig").defaults()
      require "configs.lspconfig"
    end,
  },
  -- Treesitter compatibility
  {
    "nvim-treesitter/nvim-treesitter",
    opts = overrides.treesitter,
  },
  -- File tree
  {
    "nvim-tree/nvim-tree.lua",
    opts = overrides.nvimtree,
  },
  -- Formatters
  {
    "stevearc/conform.nvim",
    lazy = true,
    ft = { "bash", "c", "julia", "lua", "python", "sh", "typst" },
    opts = overrides.conform,
  },
}

return plugins
