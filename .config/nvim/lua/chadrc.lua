-- @file $XDG_CONFIG_HOME/nvim/lua/chadrc.lua
--
-- NvChad ui and theme configuration.
--
-- @author J. Nolan Faught <nagefire.dev@gmail.com>
-- @date 18 July 2024
local M = {}

M.ui = {
  theme = "monekai",

  hl_override = {
    Comment = { italic = true },
    ["@comment"] = { italic = true },
  },
  hl_add = { NvimTreeOpenedFolderName = { fg = "green", bold = true } },
  tabufline = {
    enabled = true,
    lazyload = true,
    order = { "treeOffset", "buffers", "tabs" },
  },

  nvdash = {},
}

return M
