-- @file $XDG_CONFIG_HOME/nvim/lua/configs/overrides.lua
--
-- Custom overrides for NvChad built-in plugins.
--
-- @author J. Nolan Faught <nagefire.dev@gmail.com>
-- @date 24 July 2024
local M = {}

M.conform = {
  formatters_by_ft = {
    bash = { "shfmt" },
    c = { "clang_format" },
    lua = { "stylua" },
    python = { "black" },
    sh = { "shfmt" },
  },

  default_format_opts = { lsp_format = "fallback" },

  format_on_save = {
    timeout_ms = 500,
    lsp_format = "fallback",
  },
}

-- add parsers to install
M.treesitter = {
  ensure_installed = {
    "bash",
    "c",
    "hyprlang",
    "json",
    "julia",
    "lua",
    "markdown",
    "markdown_inline",
    "python",
    "typst",
  },
}

-- git support in nvimtree
M.nvimtree = {
  git = {
    enable = true,
  },

  renderer = {
    highlight_git = true,
    icons = {
      show = {
        git = true,
      },
    },
  },
}

return M
