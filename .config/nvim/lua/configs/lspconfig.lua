-- @file $XDG_CONFIG_HOME/nvim/lua/configs/lspconfig.lua
--
-- nvim-lspconfig settings to be handled on load.
--
-- @author J. Nolan Faught <nagefire.dev@gmail.com>
-- @date 24 Oct 2023
local on_attach = require("nvchad.configs.lspconfig").on_attach
local capabilities = require("nvchad.configs.lspconfig").capabilities
local lspconfig = require "lspconfig"

-- default config for language servers
local servers = { "bashls", "clangd", "julials", "pyright" }

for _, lsp in ipairs(servers) do
  lspconfig[lsp].setup {
    on_attach = on_attach,
    capabilities = capabilities,
  }
end

lspconfig.tinymist.setup {
  on_attach = on_attach,
  capabilities = capabilities,
  settings = { exportPdf = "onType" },
}
