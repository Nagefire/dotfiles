-- @file $XDG_CONFIG_HOME/nvim/lua/mappings.lua
--
-- NvChad mapping overrides and custom mappings.
--
-- @author J. Nolan Faught <nagefire.dev@gmail.com>
-- @date 23 July 2024
require("nvchad.mappings")

local unmap = vim.keymap.del
local map = vim.keymap.set

map("n", ";", ":", { desc = "CMD enter command mode" })

map("n", "<leader>fs", "<cmd> Telescope treesitter <CR>", { desc = "Search symbols" })

map("n", "<leader>,", "<cmd> Telescope buffers <CR>", { desc = "Switch buffer" })
map("n", "<leader>wh", "<cmd> sp <CR>", { desc = "Horizontal split" })
map("n", "<leader>wv", "<cmd> vsp <CR>", { desc = "Vertical split" })
map("n", "<leader>wc", "<C-w>c", { desc = "Close window" })

unmap("n", "<leader>wk")
map("n", "<leader>wk", "<cmd> Telescope keymaps <CR>", { desc = "Search keybindings" })
