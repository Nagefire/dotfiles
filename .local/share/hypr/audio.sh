#!/bin/bash

# @file $XDG_DATA_HOME/fkeys/audio.sh
#
# Scripts for audio function keys that send notifications to dunst.
#
# @author J. Nolan Faught
# @date 02 March 2023

# Default values for dunst
TIME=750
PID=2593
URGENCY=normal
# Icons
ICON_SRC_MUTE=microphone-sensitivity-muted-symbolic
ICON_SINK_MUTE=audio-volume-muted-symbolic
ICON_SINK_RAMP=(
	audio-volume-low-symbolic
	audio-volume-medium-symbolic
	audio-volume-high-symbolic
)
ICON_SRC_RAMP=(
	microphone-sensitivity-low-symbolic
	microphone-sensitivity-medium-symbolic
	microphone-sensitivity-high-symbolic
)

function vol_lower {
	amixer sset Master 5%-

	vol=$(amixer get Master | rg -o -m 1 '[0-9]{1,3}%' | cut -d '%' -f 1)
	msg=$(seq -s '─' $(($vol / 4)) | sed 's/[0-9]//g')
	icon=${ICON_SINK_RAMP[$(($vol / 34))]}

	dunstify -i $icon -r $PID -t $TIME "$msg"
}

function vol_raise {
	amixer sset Master 5%+

	vol=$(amixer get Master | rg -o -m 1 '[0-9]{1,3}%' | cut -d '%' -f 1)
	msg=$(seq -s '─' $(($vol / 4)) | sed 's/[0-9]//g')
	icon=${ICON_SINK_RAMP[$(($vol / 34))]}

	dunstify -i $icon -r $PID -t $TIME "$msg"
}

function toggle_sink {
	amixer sset Master toggle
	# TODO: In the case of multiple sinks/sources, get the name of the
	# sink/source in use
	if amixer get Master | rg -o -m 1 '\[on\]'; then
		vol=$(amixer get Master | rg -o -m 1 '[0-9]{1,3}' | cut -d '%' -f 1)
		icon=${ICON_SINK_RAMP[$(($vol / 34))]}
		msg=" Unmute"
	else
		icon=$ICON_SINK_MUTE
		msg=" Mute"
	fi

	dunstify -i $icon -r $PID -t $TIME $msg
}

function toggle_src {
	amixer sset Capture toggle

	if amixer get Capture | rg -o -m 1 '\[on\]'; then
		vol=$(amixer get Capture | rg -o -m 1 '[0-9]{1,3}' | cut -d '%' -f 1)
		icon=${ICON_SRC_RAMP[$(($vol / 34))]}
		msg=" Unmute"
	else
		icon=$ICON_SRC_MUTE
		msg=" Mute"
	fi

	dunstify -i $icon -r $PID -t $TIME $msg
}

$@
