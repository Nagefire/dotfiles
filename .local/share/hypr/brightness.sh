#!/usr/bin/bash

# Dunst
TIME=750
PID=2593
URGENCY=normal
# Icons
ICON=display-brightness-symbolic

function raise {
	light -A 5

	brightness=$(light -G | cut -d'.' -f1)
	echo "$brightness"
	msg=$(seq -s '─' $(($brightness / 4)) | sed 's/[0-9]//g')

	dunstify -i $ICON -r $PID -t $TIME " $msg"
}

function lower {
	light -U 5

	brightness=$(light -G | cut -d'.' -f1)
	msg=$(seq -s '─' $(($brightness / 4)) | sed 's/[0-9]//g')

	dunstify -i $ICON -r $PID -t $TIME " $msg"
}

$@
