#!/bin/bash

# @file $XDG_DATA_HOME/fkeys/playerctl.sh
#
# Scripts for mapping media function keys to playerctl bindings that send
# notifications to dunst.
#
# @author J. Nolan Faught <nagefire.dev@gmail.com>
# @date 6 Apr 2024

# Default values for dunst
TIME=750
PID=2593
URGENCY=normal
ERR_MSG="No players can perform the specified action"
# Icons
ICON_ERR=action-unavailable-symbolic
ICON_STOP=
ICON_PLAY=media-playback-start-symbolic
ICON_PAUSE=media-playback-pause-symbolic
ICON_NEXT=media-skip-forward-symbolic
ICON_PREV=media-skip-backward-symbolic

function play-pause {
	if ! playerctl play-pause; then
		dunstify -i $ICON_ERR -r $PID -t $TIME -u $URGENCY "$ERR_MSG"
		exit 0
	fi

	artist=$(playerctl metadata --format '{{ artist }}')
	title=$(playerctl metadata --format '{{ title }}')

	if [[ "$(playerctl status)" == "Paused" ]]; then
		icon=$ICON_PAUSE
	else
		icon=$ICON_PLAY
	fi

	if [ -z "$artist" ]; then
		dunstify -i $icon \
			-r $PID \
			-t $TIME \
			-u $URGENCY \
			"$title"
	else
		dunstify -i $icon \
			-r $PID \
			-t $TIME \
			-u $URGENCY \
			"$title" "$artist"
	fi
}

function stop {
	artist=$(playerctl metadata --format '{{ artist }}')
	title=$(playerctl metadata --format '{{ title }}')

	if ! playerctl stop; then
		dunstify -i $ICON_ERR -r $PID -t $TIME -u $URGENCY "$ERR_MSG"
	elif [ -z "$artist" ]; then
		dunstify -i $ICON_STOP \
			-r $PID \
			-t $TIME \
			-u $URGENCY \
			"$title" "$artist$"
	else
		dunstify -i $ICON_STOP -r $PID -t $TIME -u $URGENCY "$title"
	fi
}

function prev {
	if ! playerctl previous; then
		dunstify -i $ICON_ERR -r $PID -t $TIME -u $URGENCY "$ERR_MSG"
		exit 0
	fi

	artist=$(playerctl metadata --format '{{ artist }}')
	title=$(playerctl metadata --format '{{ title }}')

	if [ -z "$artist" ]; then
		dunstify -i $ICON_PREV \
			-r $PID \
			-t $TIME \
			-u $URGENCY \
			"$title" "$artist"
	else
		dunstify -i $ICON_PREV -r $PID -t $TIME -u $URGENCY "$title"
	fi
}

function next {
	if ! playerctl next; then
		dunstify -i $ICON_ERR -r $PID -t $TIME -u $URGENCY "$ERR_MSG"
		exit 0
	fi

	artist=$(playerctl metadata --format '{{ artist }}')
	title=$(playerctl metadata --format '{{ title }}')

	if [ -z "$artist" ]; then
		dunstify -i $ICON_NEXT \
			-r $PID \
			-t $TIME \
			-u $URGENCY \
			"$title" "$artist"
	else
		dunstify -i $ICON_NEXT -r $PID -t $TIME -u $URGENCY "$title"
	fi
}

$@
