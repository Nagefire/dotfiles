#!/bin/bash
# @file $HOME/.local/bin/gnvim
#
# Graphical session for neovim that acts as a unique application. Launching
# gnvim for the first time will open the file(s) in a new session, while
# subsequent calls will open the file(s) in the running session.
#
# @author J. Nolan Faught <nagefire.dev@gmail.com>
# @date 06 Jan 2025

: ${XDG_RUNTIME_DIR:=/run/user/$UID}

sock=$XDG_RUNTIME_DIR/gnvim.sock
term=footclient
files=""

if [ $# -gt 0 ]; then
	files=$(realpath "$@")
fi

if [ ! -S $sock ]; then
	($term --app-id gnvim \
		--title gnvim \
		-- nvim --listen $sock $files \
		>/dev/null 2>/dev/null &)
elif [ $# -ge 1 ]; then
	(nvim --server $sock \
		--remote $files \
		>/dev/null 2>/dev/null)
fi
